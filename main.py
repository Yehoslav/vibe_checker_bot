"""
The APACHE License (APACHE)

Copyright (c) 2023 Yehoslav Rudenco. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from random import choice
import logging

import telebot
import telebot.types as T
from decouple import config

from modules.denounce import register_denounce

TOKEN = config("TG_TOKEN")
logger = telebot.logger
logger.setLevel(logging.INFO)

bot = telebot.TeleBot(TOKEN)


@bot.message_handler(commands=["henlo", "gm"])
def henlo_fren(msg: T.Message):
    """Sends a help message to the user."""
    stick_pack = choice(("gmfren2", "gmfrengm"))
    fren_sticks = bot.get_sticker_set(stick_pack).stickers

    stick = choice(fren_sticks)
    bot.send_sticker(msg.chat.id, sticker=stick.file_id)


@bot.message_handler(commands=["help"])
def send_help(message) -> None:
    """Send a help message."""
    bot.reply_to(message,
                 "The following commands are available\n" +
                 "\t/henlo | /gm - send a gm sticker\n" +
                 "\t/help - display this help message.")


def main():
    "The main function."

    register_denounce(bot)
    bot.infinity_polling(allowed_updates=["message", "chat_member", "callback_query"])


if __name__ == "__main__":
    main()
