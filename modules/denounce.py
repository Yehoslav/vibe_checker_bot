"""
The APACHE License (APACHE)

Copyright (c) 2023 Yehoslav Rudenco. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import re
from random import choice

from telebot import TeleBot
from telebot.util import quick_markup
import telebot.types as T

from .config import BOT_CONST


# Denounce predefined constants
DCONST = BOT_CONST["denounce"]

def callback_denounce(call: T.CallbackQuery, bot: TeleBot, ) -> None:
    """Callback function to be called when denounce or refuse are clicked by a chat member."""
    if call.message:
        action, user_id = call.data.split("-")
        user_id = int(user_id)
        user_name = (
            "@" + call.from_user.username
            if call.from_user.username
            else call.from_user.first_name
        )

        try:
            if user_id != call.from_user.id:
                if action == "denounce":
                    bot.answer_callback_query(
                        call.id,
                        f"Proud of you {user_name}, but the test is not for you.",
                        show_alert=True,
                    )
                return

            if action == "denounce":
                bot.send_message(
                    call.message.chat.id,
                    DCONST["on_denounce"].format(username=user_name),
                )
                bot.restrict_chat_member(
                    call.message.chat.id,
                    user_id,
                    can_send_messages=True,
                    can_send_media_messages=True,
                    can_send_polls=True,
                    can_send_other_messages=True,
                    can_add_web_page_previews=True,
                )
                bot.delete_message(call.message.chat.id, call.message.id)
                return

            if action == "refuse":
                bot.send_message(
                    call.message.chat.id,
                    DCONST["on_refuse"].format(username=user_name),
                )
                bot.ban_chat_member(call.message.chat.id, user_id=user_id)
                bot.delete_message(call.message.chat.id, call.message.id)
                print(f"{call.from_user.username} clicked refuse")
                return

        except Exception as err:
            print("The following exception was caught when baning the user:", err)



def denounce(cmu: T.ChatMemberUpdated, bot: TeleBot, ):
    """Ask new chat members to denounce talmud."""

    user = cmu.from_user

    if user.id != cmu.new_chat_member.user.id:
        return

    bot.restrict_chat_member(cmu.chat.id, user.id)
    user_name = (
        "@" + user.username
        if user.username
        else user.first_name
    )

    inline_markup = quick_markup(
        {
            DCONST["denounce_btn"]: {
                "callback_data": f"denounce-{user.id}"
            },
            DCONST["refuse_btn"]: {"callback_data": f"refuse-{user.id}"},
            DCONST["talmud_btn"]: {
                "callback_data": f"denounce-help-{user.id}"
            },
        }
    )

    bot.send_photo(
        cmu.chat.id,
        DCONST["img_id"],
        caption=DCONST["caption"].format(user_name),
        reply_markup=inline_markup,
    )


def callback_denounce_help(call: T.CallbackQuery, bot: TeleBot, ) -> None:
    """Sends a message explaining why one should denounce Talmud."""
    if call.message:
        verse = choice(DCONST["talmud_verses"])
        bot.reply_to(
            call.message,
            DCONST["what_is_talmud"]+f"«{verse}»",
            parse_mode="html"
        )
        bot.edit_message_reply_markup(
            call.message.chat.id,
            call.message.id,
            reply_markup=quick_markup(
                {
                    DCONST["denounce_btn"]: {
                        "callback_data": f"denounce-{call.data.split('-')[-1]}"
                    },
                    DCONST["refuse_btn"]: {
                        "callback_data": f"refuse-{call.data.split('-')[-1]}"
                    },
                }
            ),
        )


def register_denounce(bot: TeleBot) -> None:
    """Register all the denounce function."""

    bot.register_callback_query_handler(
        callback=callback_denounce_help,
        func=lambda call: "denounce-help" in call.data,
        pass_bot=True,
    )

    bot.register_callback_query_handler(
        callback=callback_denounce,
        func=lambda call: re.search(r"^(denounce|refuse)-*\d", call.data) is not None,
        pass_bot=True,
    )

    bot.register_chat_member_handler(
        callback=denounce,
        func=lambda cmu: not cmu.old_chat_member.is_member,
        pass_bot=True,
    )
